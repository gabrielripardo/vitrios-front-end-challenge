# Feedback do projeto

## O que fiz
    A estrutura do projeto está separada por pastas que assumem reposabilidades, como de componentes, páginas, dados e modelos.
    Criei a estrutura de estilização com o Sass, criei um componente para os catálogo e para o newsletter e utilizei recursos do Sass para deixar a página como está no layout demonstrativo.    

## O que não consegui fazer
    Inicialmente eu não estava conseguindo instalar todas as dependências do projeto base, depois de algumas horas eu decidi que não compensaria ficar perdendo tempo apenas tentando fazer o projeto rodar. Então decidi que era melhor começar um novo projeto com o npx create-react-app.     

    As urls das imagens do catolog.json estavam direcionando para endereço inválido, portanto, utilizei somente uma imagem local dentro dos assets para mostrar em todos os cards.

    No rodapé, aonde se encontra a newsletter eu fiz uma validação muito simples usando o próprio html, eu tinha plano de implementar uma validação mais complexa utilizando regex e Javascript, mas devido ao tempo ser curto eu dei prioridade a construção do layout.  

## Considerações finais sobre o projeto
    No endpoint, tive dificuldade pra entender a estrutura, pois o json era muito grande. No entanto, no decorrer do desenvolvimento fui me familiarizado com o mapeamento dos dados, consegui extrair as informações necessárias para o card do catálogo e utilizar a interface para tipar os dados no array dos relógios. 
    Esse projeto fez eu praticar as boas práticas com Sass e fez eu entender o a importância de utilização de interfaces. 