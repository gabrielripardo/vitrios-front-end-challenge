import "./catalog.scss";
import Clock from "../../models/clock.model";
import ClockImage from "../../assets/images/clock-example.jpg";

export default function Catalog({ item }: { item: Clock }) {
  console.log(item);
  return (
    <div className="card">
      {/* <img className="clock-image" src={item.image} alt={item.name} /> */}
      <img className="clock-image" src={ClockImage} alt={item.name} />
      <p className="name">{item.name}</p>

      {!item.sobconsulta && (
        <>
          <p className="price">{`R$ ${item.price}`}</p>
          <p className="installment">{`10x de R$ ${item.installment} sem juros`}</p>
        </>
      )}

      <button className="btn-buy btn-hide">
        <span className="title">
          {item.sobconsulta ? "consulte" : "comprar"}
        </span>
      </button>
    </div>
  );
}
