import "./newsletter.scss";

export default function Newsletter() {
  return (
    <footer className="newsletter">
      <h1 className="title">Assine nossa newsletter</h1>
      <p className="description">
        Fique por dentro das nossas novidades e recebe 10% de desconto na
        primeira compra.
      </p>
      <p className="subdescription">
        * válido somente para jóias e não acumulativo com outras promoções
      </p>
      <form className="forms" action="">
        <input
          className="form-field"
          type="text"
          name="name"
          id="name"
          placeholder="Seu nome"
          required
          maxLength={50}
        />
        <input
          className="form-field"
          type="email"
          name="email"
          id="email"
          placeholder="Seu e-mail"
          required
          size={64}
          maxLength={64}
        />
        <button className="form-button btn-black" type="submit">
          <span className="btn-title">enviar</span>
        </button>
      </form>
    </footer>
  );
}
