import Catalog from "../../components/catalog/Catalog";
import Newsletter from "../../components/newsletter/Newsletter";
import products from "../../data/catalogo.json";
import Clock from "../../models/clock.model";

export default function Home() {
  console.log(products);

  let clocks: Clock[] = products.map(obj => {
    return {
      id: obj.productId,
      // image: obj.items[0].images[0].imageUrl, //URL das imagens estão quebradas!
      name: obj.productName,
      sobconsulta: obj.sobconsulta[0] == "sim" ? true : false,
      price: obj.items[0].sellers[0].commertialOffer.Price,
      installment: obj.items[0].sellers[0].commertialOffer.Price / 10,
    };
  });

  return (
    <div className="container">
      <div className="cards">
        {clocks.map(clock => (
          <Catalog item={clock} />
        ))}
      </div>
      <Newsletter />
    </div>
  );
}
