export default interface Clock {
  id: string;
  name: string;
  sobconsulta: boolean;
  image?: string;
  price?: number;
  installment?: number;
}
