import Home from "./pages/home";
import "./App.scss";

function App() {
  return (
    <>
      <Home />
    </>
  );
}

export default App;
